﻿using System;
using System.ComponentModel;
namespace Evaluacion.UI.Models
{
    public class LstPersonaModel
    {
        public int IdPersona { get; set; }
        [DisplayName("Nombres y Apellidos")]
        public string NombreCompleto { get; set; }
        public char Sexo { get; set; }
        [DisplayName("Ingresos")]
        public decimal Ingresos { get; set; }
        [DisplayName("Fecha Nacimiento")]
        public string FechaNacimiento { get; set; }
        [DisplayName("Estatus Actual")]
        public string EstatusActual { get; set; }
        [DisplayName("Localidad")]
        public string Localidad { get; set; }
        [DisplayName("SubLocalidad")]
        public string Sublocalidad { get; set; }
    }
}