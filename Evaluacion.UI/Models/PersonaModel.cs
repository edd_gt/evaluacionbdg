﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Evaluacion.UI.Models
{
    public class PersonaModel
    {
        [ScaffoldColumn(false)]
        public int IdPersona { get; set; }

        [Required]
        public string Nombres { get; set; }
        [Required]
        public string Apellidos { get; set; }
        [Required]
        public char Sexo { get; set; }
        [Required]
        [DisplayName("Fecha de nacimiento")]
        public DateTime FechaNacimiento { get; set; }

        [DisplayName("Estudia")]
        public bool EstatusEstudia { get; set; }
        [DisplayName("Trabaja")]
        public bool EstatusTrabaja { get; set; }
        public decimal Ingresos { get; set; }
        [Required]
        public char Localidad { get; set; }
        public string Sublocalidad { get; set; }
        public string Comentarios { get; set; }
        [Required]
        public System.Web.HttpPostedFileBase Foto { get; set; }
        [DisplayName("Foto")]
        public string UrlFoto { get; set; }
    }
}