﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Evaluacion.UI.Models
{
    public class Mensaje
    {
        public bool esError { get; set; }
        public string Texto { get; set; }
    }
}