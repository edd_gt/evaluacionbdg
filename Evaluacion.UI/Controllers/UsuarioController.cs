﻿using System;
using System.CodeDom;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Evaluacion.DTOs.Business;
using Evaluacion.DTOs.UI;
using Evaluacion.UI.Models;
using Evaluacion.UI.Utilidades;
using Newtonsoft.Json;

namespace Evaluacion.UI.Controllers
{
    public class UsuarioController : Controller
    {
        private string _direccionServicio = string.Empty;


        public UsuarioController()
        {
            _direccionServicio = ConfigurationManager.AppSettings["UrlWebApi"];
        }

        [HttpGet]
        public async Task<ActionResult> Index()
        {
            //DTO devuelto por el api
            IEnumerable<Persona> personas = null;

            //Modelo para generar la tabla con los datos de los perfiles
            IEnumerable<LstPersonaModel> listaPersonas = new List<LstPersonaModel>(); ;

            try
            {
                //Llamada al web api
                using (HttpClient cliente = new HttpClient())
                {
                    cliente.BaseAddress = new Uri(_direccionServicio);

                    //Enviamos el nombre del controlador
                    var respuesta = cliente.GetAsync("Usuario");
                    respuesta.Wait();
                    HttpResponseMessage resultado = respuesta.Result;

                    //Validamos el estatus devuelto
                    if (resultado.IsSuccessStatusCode)
                        personas = await resultado.Content.ReadAsAsync<IEnumerable<Persona>>();
                    else if (resultado.StatusCode == System.Net.HttpStatusCode.NotFound)
                        ViewData["Mensaje"] = "No hay registros disponibles";
                    else if (resultado.StatusCode == System.Net.HttpStatusCode.InternalServerError)
                        ViewData["Mensaje"] = "Ocurrió un error al intentar recuperar los registros";

                }

                //Si hay registros transformamos los datos a modelo de la vista
                if (personas != null)
                {
                    listaPersonas = this.GenerarDatosTabla(personas);
                }
            }
            catch (Exception ex)
            {
                Log.GuardarError(this, ex);
                ViewData["Mensaje"] = "Ocurrió un error en la página. No se pudo cargar la información";
            }
            return View(listaPersonas);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Index(string txtNombre, string txtFechaInicio, string txtFechaFin,
            string chkMasculino, string chkFemenino, char slcLocalidad)
        {
            //DTO devuelto por el api
            IEnumerable<Persona> personas = null;

            //Modelo para generar la tabla con los datos de los perfiles
            IEnumerable<LstPersonaModel> listaPersonas = new List<LstPersonaModel>();

            try
            {
                FiltrosBusquedaPersona filtro = new FiltrosBusquedaPersona();

                //Aplicamos los filtros obtenidos
                filtro.Nombre = txtNombre;
                filtro.FechaInicio = txtFechaInicio;
                filtro.FechaFin = txtFechaFin;

                bool Masculino = bool.Parse(chkMasculino);
                bool Femenino = bool.Parse(chkFemenino);
                if (Masculino && Femenino)
                    filtro.Sexo = char.MinValue;
                else if (Masculino)
                    filtro.Sexo = 'M';
                else if (Femenino)
                    filtro.Sexo = 'F';
                else
                    filtro.Sexo = char.MinValue;

                filtro.Localidad = (slcLocalidad == 'T') ? char.MinValue : slcLocalidad;

                var jsonFiltro = JsonConvert.SerializeObject(filtro);
                HttpContent contenido = new StringContent(jsonFiltro, Encoding.UTF8, "application/json");

                //Llamada al web api
                using (HttpClient cliente = new HttpClient())
                {
                    cliente.BaseAddress = new Uri(_direccionServicio);


                    //Enviamos el nombre del controlador
                    var respuesta = cliente.PostAsync("Usuario/Buscar", contenido);
                    respuesta.Wait();
                    HttpResponseMessage resultado = respuesta.Result;

                    //Validamos el estatus devuelto
                    if (resultado.IsSuccessStatusCode)
                        personas = await resultado.Content.ReadAsAsync<IEnumerable<Persona>>();
                    else if (resultado.StatusCode == System.Net.HttpStatusCode.NotFound)
                        ViewData["Mensaje"] = "No se encontró ningún registro";
                    else if (resultado.StatusCode == System.Net.HttpStatusCode.InternalServerError)
                        ViewData["Mensaje"] = "Ocurrió un error al intentar recuperar los registros";

                }

                if (personas != null)
                {
                    listaPersonas = this.GenerarDatosTabla(personas);
                }
            }
            catch (Exception ex)
            {
                Log.GuardarError(this, ex);
            }
            return View(listaPersonas);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Agregar(PersonaModel personaModel)
        {
            FrmPersona persona = new FrmPersona();
            try
            {
                persona.Nombres = personaModel.Nombres;
                persona.Apellidos = personaModel.Apellidos;
                persona.FechaNacimiento = personaModel.FechaNacimiento.ToString();
                persona.Sexo = personaModel.Sexo;
                persona.EstatusEstudia = personaModel.EstatusEstudia;
                persona.EstatusTrabaja = personaModel.EstatusTrabaja;
                persona.Ingresos = personaModel.Ingresos;
                persona.Localidad = personaModel.Localidad;
                persona.Sublocalidad = personaModel.Sublocalidad;
                persona.Comentarios = personaModel.Comentarios;

                //Validamos si hay imagen
                if (personaModel.Foto.ContentLength > 0)
                {
                    string _FileName = Path.GetFileName(personaModel.Foto.FileName);
                    string _path = Path.Combine(Server.MapPath("~/Content/Fotos"), _FileName);
                    personaModel.Foto.SaveAs(_path);

                    persona.Foto = "/Content/Fotos/" + _FileName;
                }
                else
                {
                    persona.Foto = string.Empty;
                }

                var jsonFiltro = JsonConvert.SerializeObject(persona);
                HttpContent contenido = new StringContent(jsonFiltro, Encoding.UTF8, "application/json");

                //Llamada al web api    
                using (HttpClient cliente = new HttpClient())
                {
                    cliente.BaseAddress = new Uri(_direccionServicio);

                    //Enviamos el nombre del controlador
                    var respuesta = cliente.PostAsync("Usuario", contenido);
                    respuesta.Wait();
                    HttpResponseMessage resultado = respuesta.Result;

                    //Validamos el estatus devuelto
                    if (resultado.IsSuccessStatusCode)

                        TempData["MensajeRegistro"] = new Mensaje() { esError = false, Texto = "Se ha registrado el perfil exitosamente" };
                    else
                        TempData["MensajeRegistro"] = new Mensaje() { esError = true, Texto = "No se pudo agregar el perfil." };

                }
            }
            catch (Exception ex)
            {
                Log.GuardarError(this, ex);
            }

            return RedirectToAction("Index");
        }

        [HttpGet]
        public async Task<ActionResult> Editar(int id)
        {
            //DTO devuelto por el api
            FrmPersona persona = null;

            //Modelo a utilizar para la vista
            PersonaModel personaModel = new PersonaModel();
            try
            {
                //Llamada al web api
                using (HttpClient cliente = new HttpClient())
                {
                    cliente.BaseAddress = new Uri(_direccionServicio);


                    //Enviamos el nombre del controlador
                    var respuesta = cliente.GetAsync("Usuario/" + id);
                    respuesta.Wait();
                    HttpResponseMessage resultado = respuesta.Result;

                    //Validamos el estatus devuelto
                    if (resultado.IsSuccessStatusCode)
                    {
                        await this.GenerarCatalogos();
                        persona = await resultado.Content.ReadAsAsync<FrmPersona>();
                        personaModel = this.GenerarPersonaModel(persona);
                    }
                    else if (resultado.StatusCode == System.Net.HttpStatusCode.NotFound)
                        ViewData["Mensaje"] = "No se encontró ningún registro";
                    else if (resultado.StatusCode == System.Net.HttpStatusCode.InternalServerError)
                        ViewData["Mensaje"] = "Ocurrió un error al intentar recuperar los registros";

                }
            }
            catch (Exception ex)
            {
                Log.GuardarError(this, ex);
            }
            return View(personaModel);
        }

        private IEnumerable<LstPersonaModel> GenerarDatosTabla(IEnumerable<Persona> personas)
        {
            List<LstPersonaModel> lstPersonas = new List<LstPersonaModel>();
            foreach (Persona persona in personas)
            {
                LstPersonaModel item = new LstPersonaModel()
                {
                    IdPersona = persona.IdPersona,
                    NombreCompleto = persona.Nombres + " " + persona.Apellidos,
                    Sexo = persona.Sexo,
                    Ingresos = persona.Ingresos,
                    FechaNacimiento = (DateTime.Parse(persona.FechaNacimiento)).ToString("dd/MM/yyyy"),
                };

                switch (persona.EstatusActualId)
                {
                    case (int)Estatus.Estudiando:
                        item.EstatusActual = "Estudiando";
                        break;
                    case (int)Estatus.Trabajando:
                        item.EstatusActual = "Trabajando";
                        break;
                    case (int)Estatus.Ambos:
                        item.EstatusActual = "Ambos";
                        break;
                    default:
                        item.EstatusActual = "Ninguno";
                        break;
                }

                switch (persona.LocalidadId)
                {
                    case (char)Localidades.Local:
                        item.Localidad = "Local";
                        break;
                    case (char)Localidades.Extranjero:
                        item.Localidad = "Extranjero";
                        break;
                }

                switch (persona.SublocalidadId)
                {
                    case "LG":
                        item.Sublocalidad = "Gubernamental";
                        break;
                    case "LP":
                        item.Sublocalidad = "Privada";
                        break;
                    case "EE":
                        item.Sublocalidad = "Exenta";
                        break;
                    case "EG":
                        item.Sublocalidad = "Gravado";
                        break;
                }

                lstPersonas.Add(item);
            }

            return lstPersonas;
        }

        private PersonaModel GenerarPersonaModel(FrmPersona persona)
        {
            PersonaModel personaModel = new PersonaModel();

            personaModel.IdPersona = persona.IdPersona;
            personaModel.Nombres = persona.Nombres;
            personaModel.Apellidos = persona.Apellidos;
            personaModel.Sexo = persona.Sexo;
            personaModel.FechaNacimiento = DateTime.Parse(persona.FechaNacimiento);
            personaModel.Ingresos = persona.Ingresos;
            personaModel.Localidad = persona.Localidad;
            personaModel.Sublocalidad = persona.Sublocalidad;
            personaModel.Comentarios = persona.Comentarios;
            personaModel.UrlFoto = persona.Foto;
            personaModel.EstatusEstudia = persona.EstatusEstudia;
            personaModel.EstatusTrabaja = persona.EstatusTrabaja;

            return personaModel;
        }

        private async Task GenerarCatalogos()
        {
            Catalogos catalogos = new Catalogos();

            using (HttpClient cliente = new HttpClient())
            {
                cliente.BaseAddress = new Uri(_direccionServicio);


                //Enviamos el nombre del controlador
                var respuesta = cliente.GetAsync("Ui/");
                respuesta.Wait();
                HttpResponseMessage resultado = respuesta.Result;

                //Validamos el estatus devuelto
                if (resultado.IsSuccessStatusCode)
                {
                    catalogos = await resultado.Content.ReadAsAsync<Catalogos>();

                    var Localidades = from catalogo in catalogos.Localidad
                                      select new SelectListItem()
                                      {
                                          Value = catalogo.id,
                                          Text = catalogo.descripcion

                                      };
                    var Sublocalidades = from catalogo in catalogos.Sublocalidad
                                         select new SelectListItem()
                                         {
                                             Value = catalogo.id,
                                             Text = catalogo.descripcion

                                         };
                    ViewData["LstLocalidades"] = Localidades;
                    ViewData["LstSublocalidades"] = Sublocalidades;
                }
                else if (resultado.StatusCode == System.Net.HttpStatusCode.NotFound)
                    ViewData["Mensaje"] = "No se encontró ningún catálogo ";
                else if (resultado.StatusCode == System.Net.HttpStatusCode.InternalServerError)
                    ViewData["Mensaje"] = "Ocurrió un error al intentar recuperar los registros";

            }
        }
    }
}