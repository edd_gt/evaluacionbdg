﻿using System;
using System.Configuration;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.Web;

namespace Evaluacion.WebApi.Utilidades
{
    public class Log
    {
        public static void GuardarError(object obj, Exception ex)
        {
            StackTrace stacktrace = new StackTrace();
            Log log = new Log();

            RegistrarFile(obj, ex);

            ////if (!log.RegistrarEnBD(obj.GetType().FullName, stacktrace.GetFrame(1).GetMethod().Name, ex.Message, ex.StackTrace))
            ////{
            ////    RegistrarFile(obj, ex);
            ////}
        }

        ////private bool RegistrarEnBD(string objeto, string metodo, string mensaje, string stacktrace)
        ////{
        ////    string _connString = ConfigurationManager.ConnectionStrings["cadenaConexionBd"].ConnectionString;

        ////    bool Respuesta = false;

        ////    try
        ////    {
        ////        using (SqlCommand cmd = new SqlCommand()
        ////        {
        ////            CommandType = System.Data.CommandType.StoredProcedure,
        ////            CommandText = "dbo.RegistrarErrorApp",
        ////            Connection = new SqlConnection(_connString)
        ////        })
        ////        {
        ////            cmd.Parameters.AddWithValue("Objeto", objeto);
        ////            cmd.Parameters.AddWithValue("Metodo", metodo);
        ////            cmd.Parameters.AddWithValue("Mensaje", mensaje);
        ////            cmd.Parameters.AddWithValue("StackTrace", stacktrace);

        ////            cmd.Connection.Open();

        ////            cmd.ExecuteNonQuery();

        ////            cmd.Connection.Close();
        ////            Respuesta = true;
        ////        }
        ////    }
        ////    catch (Exception ex)
        ////    {
        ////        Respuesta = false;
        ////        RegistrarFile(this, ex);
        ////    }

        ////    return Respuesta;
        ////}

        public static void RegistrarFile(object obj, Exception ex)
        {
            string fecha = System.DateTime.Now.ToString("yyyyMMdd");
            string hora = System.DateTime.Now.ToString("HH:mm:ss");
            string path = HttpContext.Current.Request.MapPath("~/log/" + fecha + ".txt");

            StreamWriter sw = new StreamWriter(path, true);

            StackTrace stacktrace = new StackTrace();
            sw.WriteLine(obj.GetType().FullName + " " + hora);
            sw.WriteLine(stacktrace.GetFrame(1).GetMethod().Name + " - " + ex.Message);
            sw.WriteLine("Detalle error: \n" + ex.StackTrace);
            sw.WriteLine("");
            sw.Flush();
            sw.Close();
        }
    }
}