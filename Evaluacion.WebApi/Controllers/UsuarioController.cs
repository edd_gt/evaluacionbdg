﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Routing;
using Evaluacion.BusinessLayer;
using Evaluacion.DTOs.Business;
using Evaluacion.DTOs.UI;
using Evaluacion.WebApi.Utilidades;

namespace Evaluacion.WebApi.Controllers
{
    public class UsuarioController : ApiController
    {
        IUsuario usuario;

        [HttpGet]
        public IHttpActionResult ConsultarPersonas()
        {
            try
            {
                usuario = new Usuario();

                IEnumerable<Persona> personas = usuario.ConsultarPersona(new FiltrosBusquedaPersona());

                if (personas != null)
                    return Ok(personas);
                else
                    return NotFound();

            }
            catch (Exception ex)
            {
                Log.GuardarError(this, ex);
                return InternalServerError();
            }
            
        }

        [HttpPost]
        [Route("api/Usuario/Buscar")]
        public IHttpActionResult ConsultarPersonas([FromBody] FiltrosBusquedaPersona filtro)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    usuario = new Usuario();

                    IEnumerable<Persona> personas = usuario.ConsultarPersona(filtro);

                    if (personas != null)
                        return Ok(personas);
                    else
                        return NotFound();

                }
                else
                {
                    return BadRequest("Formato de filtro inválido");
                }

            }
            catch (Exception ex)
            {
                Log.GuardarError(this, ex);
                return InternalServerError();
            }

        }

        [HttpGet]
        [Route("api/Usuario/{idPersona}")]
        public IHttpActionResult ConsultarPersonas(int idPersona)
        {
            try
            {
                usuario = new Usuario();

                FrmPersona persona = usuario.ConsultarPersona(idPersona);

                if (persona != null)
                    return Ok(persona);
                else
                    return NotFound();

            }
            catch (Exception ex)
            {
                Log.GuardarError(this, ex);
                return InternalServerError();
            }

        }

        [HttpPost]
        public HttpResponseMessage RegistrarPerfil([FromBody] FrmPersona frmPersona)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    usuario = new Usuario();
                    bool exito = usuario.RegistrarPersona(frmPersona);

                    if (exito)
                    {
                        return new HttpResponseMessage()
                        {
                            StatusCode = HttpStatusCode.OK,
                            ReasonPhrase = HttpUtility.HtmlEncode("La persona a sido registrada correctamente.")
                        };
                    }
                    else
                    {
                        return new HttpResponseMessage()
                        {
                            StatusCode = HttpStatusCode.InternalServerError,
                            ReasonPhrase = HttpUtility.HtmlEncode("No se pudo registrar a la persona")
                        };
                    }
                }
                else
                {
                    return new HttpResponseMessage()
                    {
                        StatusCode = HttpStatusCode.BadRequest,
                        ReasonPhrase = HttpUtility.HtmlEncode("Estructura no correcta.")
                    };
                }
            }
            catch (Exception ex)
            {
                Log.GuardarError(this, ex);
                return new HttpResponseMessage()
                {
                    StatusCode = HttpStatusCode.InternalServerError,
                    ReasonPhrase = HttpUtility.HtmlEncode("Ocurrio un error a intentar registrar a la persona.")
                };
            }

        }

        [HttpPut]
        [Route("api/Usuario/{idPersona}")]
        public HttpResponseMessage ActualizarPerfil(int idPersona, [FromBody] FrmPersona frmPersona)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    frmPersona.IdPersona = idPersona;
                    usuario = new Usuario();
                    bool exito = usuario.ActualizarDatosPersona(frmPersona);

                    if (exito)
                    {
                        return new HttpResponseMessage()
                        {
                            StatusCode = HttpStatusCode.OK,
                            ReasonPhrase = HttpUtility.HtmlEncode("Los datos de la persona han sido actualizados correctamente.")
                        };
                    }
                    else
                    {
                        return new HttpResponseMessage()
                        {
                            StatusCode = HttpStatusCode.InternalServerError,
                            ReasonPhrase = HttpUtility.HtmlEncode("No se pudo actualizar los datos de la persona.")
                        };
                    }
                }
                else
                {
                    return new HttpResponseMessage()
                    {
                        StatusCode = HttpStatusCode.BadRequest,
                        ReasonPhrase = HttpUtility.HtmlEncode("Estructura de petición no correcta.")
                    };
                }
            }
            catch (Exception ex)
            {
                Log.GuardarError(this, ex);
                return new HttpResponseMessage()
                {
                    StatusCode = HttpStatusCode.InternalServerError,
                    ReasonPhrase = HttpUtility.HtmlEncode("Ocurrio un error a intentar actualizar los datos de la persona.")
                };
            }
        }
    }
}
