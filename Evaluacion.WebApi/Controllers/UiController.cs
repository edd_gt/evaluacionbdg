﻿using System;
using System.Web;
using System.Web.Http;
using System.Web.Http.Cors;
using Evaluacion.BusinessLayer;
using Evaluacion.DTOs.UI;
using Evaluacion.WebApi.Utilidades;

namespace Evaluacion.WebApi.Controllers
{

    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class UiController : ApiController
    {
        string _connString = string.Empty;
        IUsuario usuario;

        [HttpGet]
        public IHttpActionResult ConsultarCatalogos()
        {
            try
            {
                usuario = new Usuario();
                Catalogos catalogos = usuario.ConsultarCatalogos();

                if (catalogos != null)
                {
                    return Ok(catalogos);
                }
                else
                {
                    return NotFound();
                }
            }
            catch (Exception ex)
            {
                Log.GuardarError(this, ex);
                return InternalServerError();
            }

        }
    }
}
