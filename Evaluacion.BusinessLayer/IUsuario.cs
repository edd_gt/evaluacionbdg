﻿using System;
using System.Collections.Generic;
using Evaluacion.DTOs.Business;
using Evaluacion.DTOs.UI;

namespace Evaluacion.BusinessLayer
{
    public interface IUsuario
    {
        /// <summary>
        /// Consulta los datos para llenar las opciones a mostrar para el usuario
        /// </summary>
        /// <returns>Objeto con los catálogos a mostrar</returns>
        Catalogos ConsultarCatalogos();

        /// <summary>
        /// Registra los datos de una persona en base de datos
        /// </summary>
        /// <param name="datosPersona">Datos de la persona, obtenidos en la capa de presentación</param>
        /// <returns></returns>
        bool RegistrarPersona(FrmPersona frmPersona);

        /// <summary>
        /// Consulta los datos de las personas que coincidan con los filtros
        /// </summary>
        /// <param name="filtros">Filtras a aplicar a la busqueda</param>
        /// <returns></returns>
        IEnumerable<Persona> ConsultarPersona(FiltrosBusquedaPersona filtros);

        /// <summary>
        /// Consulta los datos de una persona por medio de su identificador
        /// </summary>
        /// <param name="idPersona">Identificador de la persona</param>
        /// <returns></returns>
        FrmPersona ConsultarPersona(int idPersona);

        /// <summary>
        /// Actuliza los datos de la persona en base de datos
        /// </summary>
        /// <param name="idPersona">Identificador de la persona</param>
        /// <param name="frmPersona">Objeto con los datos nuevos</param>
        /// <returns></returns>
        bool ActualizarDatosPersona(FrmPersona frmPersona);
    }
}
