﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Sql;
using System.Linq;
using System.Runtime.Remoting.Channels;
using System.Security.Cryptography;
using Evaluacion.DataLayer;
using Evaluacion.DTOs.Business;
using Evaluacion.DTOs.UI;

/// <summary>
/// Capa de negocios de la aplicación. Contiene la lógica y las reglas de negocio para la el procesamiento de los datos
/// </summary>
namespace Evaluacion.BusinessLayer
{
    /// <summary>
    /// Contiene la lógica y las reglas de negocio para la aplicación
    /// </summary>
    public class Usuario : IUsuario
    {
        IDataAccess accesoDatos;

        public Usuario()
        {
            this.accesoDatos = new DataAccess();
        }

        public bool ActualizarDatosPersona(FrmPersona frmPersona)
        {
            bool exito = false;

            Persona persona = new Persona();

            persona.IdPersona = frmPersona.IdPersona;
            persona.Nombres = frmPersona.Nombres;
            persona.Apellidos = frmPersona.Apellidos;
            persona.FechaNacimiento = frmPersona.FechaNacimiento;
            persona.Sexo = frmPersona.Sexo;
            persona.Ingresos = frmPersona.Ingresos;
            persona.LocalidadId = frmPersona.Localidad;
            persona.SublocalidadId = frmPersona.Sublocalidad;
            persona.Comentarios = frmPersona.Comentarios ?? string.Empty;
            persona.Foto = frmPersona.Foto;

            if (frmPersona.EstatusEstudia && frmPersona.EstatusTrabaja)
            {
                persona.EstatusActualId = 3;
            }
            else if (frmPersona.EstatusTrabaja)
            {
                persona.EstatusActualId = 2;
            }
            else if (frmPersona.EstatusEstudia)
            {
                persona.EstatusActualId = 1;
            }
            else
            {
                persona.EstatusActualId = 4;
            }

            exito = (this.accesoDatos.ModificarPersona(persona) == 1) ? true : false;

            return exito;
        }

        /// <summary>
        /// Consultar los datos para llenar las opciones a mostrar para el usuario
        /// </summary>
        /// <returns>Objeto con los catálogos a mostrar</returns>
        public Catalogos ConsultarCatalogos()
        {
            Catalogos catalogos = new Catalogos();

            DataSet datosBD = new DataSet();

            datosBD = this.accesoDatos.ConsultarCatalogos();

            if (datosBD != null)
            {
                //Tabla 0: Localidad
                catalogos.Localidad = new List<ItemCatalogo>();
                foreach (DataRow item in datosBD.Tables[1].Rows)
                {
                    catalogos.Localidad.Add(new ItemCatalogo()
                    {
                        id = item["IdLocalidad"].ToString(),
                        descripcion = item["Descripcion"].ToString()
                    });
                }
                //Tabla 1: Sublocalidad
                catalogos.Sublocalidad = new List<ItemCatalogo>();
                foreach (DataRow item in datosBD.Tables[2].Rows)
                {
                    catalogos.Sublocalidad.Add(new ItemCatalogo()
                    {
                        id = item["IdSublocalidad"].ToString(),
                        descripcion = item["Descripcion"].ToString()
                    });
                }
            }
            else
            {
                catalogos = null;
            }

            return catalogos;
        }

        /// <summary>
        /// Consulta los datos de las personas que coincidan con los filtros
        /// </summary>
        /// <param name="filtros">Filtras a aplicar a la busqueda</param>
        /// <returns></returns>
        public IEnumerable<Persona> ConsultarPersona(FiltrosBusquedaPersona filtros)
        {
            IEnumerable<Persona> personas;

            DateTime fechaInicio = (!string.IsNullOrEmpty(filtros.FechaInicio)) ? DateTime.Parse(filtros.FechaInicio) : DateTime.Parse("1900-01-01 00:00:00.000");
            DateTime fechaFin = (!string.IsNullOrEmpty(filtros.FechaFin)) ? DateTime.Parse(filtros.FechaFin) : DateTime.Now;
            
            DataTable  datosBD = this.accesoDatos.ConsultarPersona(
                filtros.Nombre,
                fechaInicio,
                fechaFin,
                filtros.Sexo,
                filtros.Localidad
                );

            if (datosBD != null && datosBD.Rows.Count > 0)
            {
                //Generamos la lista de objetos 
                personas = (from fila in datosBD.AsEnumerable()
                            select new Persona()
                            {
                                IdPersona = Convert.ToInt32(fila["IdPersona"]),
                                Nombres = Convert.ToString(fila["Nombres"]),
                                Apellidos = Convert.ToString(fila["Apellidos"]),
                                Sexo = Convert.ToChar(fila["Sexo"]),
                                Ingresos = Convert.ToDecimal(fila["Ingresos"]),
                                FechaNacimiento = Convert.ToString(fila["FechaNacimiento"]),
                                EstatusActualId = Convert.ToInt16(fila["EstatusActualId"]),
                                LocalidadId = Convert.ToChar(fila["LocalidadId"]),
                                SublocalidadId = Convert.ToString(fila["SublocalidadId"]),
                                Comentarios = Convert.ToString(fila["Comentarios"]),
                                Foto = Convert.ToString(fila["Foto"])
                            }
                            ).ToList();
            }
            else
            {
                personas = null;
            }

            return personas;

        }

        /// <summary>
        /// Consulta los datos de una persona por medio de su identificador
        /// </summary>
        /// <param name="idPersona">Identificador de la persona</param>
        /// <returns></returns>
        public FrmPersona ConsultarPersona(int idPersona)
        {
            FrmPersona persona = new FrmPersona();
                    
            DataTable datosBD = this.accesoDatos.ConsultarPersona(idPersona);

            if (datosBD != null && datosBD.Rows.Count > 0)
            {
                //Generamos la lista de objetos 
                persona = (from fila in datosBD.AsEnumerable()
                            select new FrmPersona()
                            {
                                IdPersona = Convert.ToInt32(fila["IdPersona"]),
                                Nombres = Convert.ToString(fila["Nombres"]),
                                Apellidos = Convert.ToString(fila["Apellidos"]),
                                Sexo = Convert.ToChar(fila["Sexo"]),
                                Ingresos = Convert.ToDecimal(fila["Ingresos"]),
                                FechaNacimiento = Convert.ToString(fila["FechaNacimiento"]),
                                Localidad = Convert.ToChar(fila["LocalidadId"]),
                                Sublocalidad = Convert.ToString(fila["SublocalidadId"]),
                                Comentarios = Convert.ToString(fila["Comentarios"]),
                                Foto = Convert.ToString(fila["Foto"])
                            } 
                            ).FirstOrDefault();

                switch (datosBD.Rows[0]["EstatusActualId"].ToString())
                {
                    case "1":
                        persona.EstatusEstudia = true;
                        break;
                    case "2":
                        persona.EstatusEstudia = true;
                        break;
                    case "3":
                        persona.EstatusEstudia = true;
                        persona.EstatusTrabaja = true;
                        break;
                    case "4":
                        persona.EstatusEstudia = false;
                        persona.EstatusTrabaja = false;
                        break;
                }
            }
            else
            {
                persona = null;
            }

            return persona;
        }

        public bool RegistrarPersona(FrmPersona frmPersona)
        {
            bool exito = false;

            Persona persona = new Persona();

            persona.Nombres = frmPersona.Nombres;
            persona.Apellidos = frmPersona.Apellidos;
            persona.FechaNacimiento = frmPersona.FechaNacimiento;
            persona.Sexo = frmPersona.Sexo;
            persona.Ingresos = frmPersona.Ingresos;
            persona.LocalidadId = frmPersona.Localidad;
            persona.SublocalidadId = frmPersona.Sublocalidad;
            persona.Comentarios = frmPersona.Comentarios ?? string.Empty;
            persona.Foto = frmPersona.Foto;

            if (frmPersona.EstatusEstudia && frmPersona.EstatusTrabaja)
            {
                persona.EstatusActualId = 3;
            }
            else if (frmPersona.EstatusTrabaja)
            {
                persona.EstatusActualId = 2;
            }
            else if (frmPersona.EstatusEstudia)
            {
                persona.EstatusActualId = 1;
            }
            else
            {
                persona.EstatusActualId = 4;
            }

            exito = (this.accesoDatos.RegistrarPersona(persona) == 1) ? true : false;

            return exito;
        }
    }
}
