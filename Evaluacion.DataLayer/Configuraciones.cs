﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace Evaluacion.DataLayer
{
    internal class Configuraciones
    {
        public static string ObtenerConfiguracion(string nombreConfiguracion)
        {
            string configuracion = string.Empty;

            string _pathConfiguraciones = AppDomain.CurrentDomain.BaseDirectory;
            _pathConfiguraciones += @"\configuraciones.xml";
            XmlDocument xml = new XmlDocument();

            xml.Load(_pathConfiguraciones);

            configuracion = xml.SelectSingleNode("Configuraciones/" + nombreConfiguracion).InnerText;

            return configuracion;
        }
    }
}
