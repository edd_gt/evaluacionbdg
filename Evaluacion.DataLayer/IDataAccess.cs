﻿using System;
using System.Collections.Generic;
using System.Data;
using Evaluacion.DTOs.Business;

namespace Evaluacion.DataLayer
{
    /// <summary>
    /// Interfaz para acceder a los datos en base de  datos
    /// </summary>
    public interface IDataAccess
    {
        /// <summary>
        /// Permite consultar los catálogos de opciones para el usuario
        /// </summary>
        /// <returns></returns>
        DataSet ConsultarCatalogos();

        /// <summary>
        /// Registra la información de una persona en base de datos
        /// </summary>
        /// <param name="persona">Contiene la información de la persona a registrar</param>
        /// <returns></returns>
        int RegistrarPersona(Persona persona);

        /// <summary>
        /// Consulta en base de datos los datos de las personas que coincidan con los filtros
        /// </summary>
        /// <param name="nombre">Filtro de nombre(Nombres y/o Apellidos)</param>
        /// <param name="fechaInicio">Filtro rango de fechas de nacimiento</param>
        /// <param name="fechaFin">Filtro rango de fechas de nacimiento</param>
        /// <param name="sexo"> Filtro de sexo de la persona</param>
        /// <param name="localidadId">Filtro de localidad de la persona</param>
        /// <returns></returns>
        DataTable ConsultarPersona(string nombre, DateTime fechaInicio, DateTime fechaFin, char sexo, char localidadId);

        /// <summary>
        /// Consulta en base de dastos los datos de una persona según su identificador 
        /// </summary>
        /// <param name="idPersona">Identificador de la persona</param>
        /// <returns></returns>
        DataTable ConsultarPersona(int idPersona);

        /// <summary>
        /// Modifica losd datos de una persona en base de datos
        /// </summary>
        /// <param name="idPersona">Identificador de la persona</param>
        /// <param name="persona">Objeto con los datos modificados de la persona</param>
        /// <returns></returns>
        int ModificarPersona(Persona persona);
    }
}
