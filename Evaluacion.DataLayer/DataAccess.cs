﻿namespace Evaluacion.DataLayer
{
    using System.Data;
    using Evaluacion.DTOs.Business;
    using System.Collections.Generic;
    using System;
    using System.Data.SqlClient;

    /// <summary>
    /// Clase para permitir el acceso a base de datos
    /// </summary>
    public class DataAccess: IDataAccess
    {
        /// <summary>
        /// Cadena de conexión a BD
        /// </summary>
        private string _connString = string.Empty;

        /// <summary>
        /// Método contructor
        /// </summary>
        /// <param name="connString">String de conexión a la base de datos</param>
        public DataAccess()
        {
            this._connString = Configuraciones.ObtenerConfiguracion("CadenaConexionBD");
        }

        /// <summary>
        /// Permite consultar los catálogos de opciones para el usuario
        /// </summary>
        /// <returns></returns>
        public DataSet ConsultarCatalogos()
        {
            DataSet datosBD = new DataSet();

            using (SqlCommand cmd = new SqlCommand()
            {
                CommandType = CommandType.StoredProcedure,
                CommandText = "dbo.[ConsultaCatalogos]",
                Connection = new SqlConnection(_connString)
            })
            {
                using (SqlDataAdapter SqlData = new SqlDataAdapter(cmd))
                {
                    SqlData.Fill(datosBD);

                    if (datosBD.Tables.Count <= 0)
                    {
                        datosBD = null;
                    }

                }
            }

            return datosBD;
        }

        /// <summary>
        /// Consulta en base de datos los datos de las personas que coincidan con los filtros
        /// </summary>
        /// <param name="nombre">Filtro de nombre(Nombres y/o Apellidos)</param>
        /// <param name="fechaInicio">Filtro rango de fechas de nacimiento</param>
        /// <param name="fechaFin">Filtro rango de fechas de nacimiento</param>
        /// <param name="sexo"> Filtro de sexo de la persona</param>
        /// <param name="localidadId">Filtro de localidad de la persona</param>
        /// <returns></returns>
        public DataTable ConsultarPersona(string nombre, DateTime fechaInicio, DateTime fechaFin, char sexo, char localidadId)
        {
            DataTable datosBD = new DataTable();

            using (SqlCommand cmd = new SqlCommand()
            {
                CommandType = CommandType.StoredProcedure,
                CommandText = "dbo.[ConsultarPersona]",
                Connection = new SqlConnection(_connString)
            })
            {
                cmd.Parameters.AddWithValue("FiltroNombre", nombre);
                cmd.Parameters.AddWithValue("FiltroFechaInicio", fechaInicio);
                cmd.Parameters.AddWithValue("FiltroFechaFin", fechaFin);

                if(sexo != char.MinValue) 
                    cmd.Parameters.AddWithValue("FiltroSexto", sexo);
                if (localidadId != char.MinValue)
                    cmd.Parameters.AddWithValue("FiltroLocalidad", localidadId);

                SqlParameter algo = new SqlParameter();

                using (SqlDataAdapter SqlData = new SqlDataAdapter(cmd))
                {
                    SqlData.Fill(datosBD);

                    if (datosBD.Rows.Count <= 0)
                    {
                        datosBD = null;
                    }

                }
            }

            return datosBD;

        }

        public DataTable ConsultarPersona(int idPersona)
        {
            DataTable datosBD = new DataTable();

            using (SqlCommand cmd = new SqlCommand()
            {
                CommandType = CommandType.StoredProcedure,
                CommandText = "dbo.[ConsultarPersona]",
                Connection = new SqlConnection(_connString)
            })
            {
                cmd.Parameters.AddWithValue("FiltroId", idPersona);

                SqlParameter algo = new SqlParameter();

                using (SqlDataAdapter SqlData = new SqlDataAdapter(cmd))
                {
                    SqlData.Fill(datosBD);

                    if (datosBD.Rows.Count <= 0)
                    {
                        datosBD = null;
                    }

                }
            }

            return datosBD;
        }

        public int ModificarPersona(Persona persona)
        {
            int exito = 0;
            DataTable datosBD = new DataTable();

            using (SqlCommand cmd = new SqlCommand()
            {
                CommandType = CommandType.StoredProcedure,
                CommandText = "dbo.[ActualizarPersona]",
                Connection = new SqlConnection(_connString)
            })
            {
                cmd.Parameters.AddWithValue("@IdPersona", persona.IdPersona);
                cmd.Parameters.AddWithValue("@Nombres", persona.Nombres);
                cmd.Parameters.AddWithValue("@Apellidos", persona.Apellidos);
                cmd.Parameters.AddWithValue("@Sexo", persona.Sexo);
                cmd.Parameters.AddWithValue("@FechaNacimiento", DateTime.Parse(persona.FechaNacimiento));
                cmd.Parameters.AddWithValue("@EstatusActualId", persona.EstatusActualId);
                cmd.Parameters.AddWithValue("@Ingresos", persona.Ingresos);
                cmd.Parameters.AddWithValue("@LocalidadId", persona.LocalidadId);
                cmd.Parameters.AddWithValue("@SublocalidadId", persona.SublocalidadId);
                cmd.Parameters.AddWithValue("@Comentarios", persona.Comentarios);
                cmd.Parameters.AddWithValue("@Foto", persona.Foto);

                using (SqlDataAdapter SqlData = new SqlDataAdapter(cmd))
                {
                    SqlData.Fill(datosBD);

                    if (datosBD.Rows.Count == 1)
                    {
                        exito = int.Parse(datosBD.Rows[0]["Exitoso"].ToString());
                    }
                }
            }

            return exito;
        }

        /// <summary>
        /// Registra la información de una persona en base de datos
        /// </summary>
        /// <param name="persona">Contiene la información de la persona a registrar</param>
        /// <returns></returns>
        public int RegistrarPersona(Persona persona)
        {
            int exito = 0;
            DataTable datosBD = new DataTable();

            using (SqlCommand cmd = new SqlCommand()
            {
                CommandType = CommandType.StoredProcedure,
                CommandText = "dbo.[RegistrarPersona]",
                Connection = new SqlConnection(_connString)
            })
            {
                cmd.Parameters.AddWithValue("@Nombres", persona.Nombres);
                cmd.Parameters.AddWithValue("@Apellidos", persona.Apellidos);
                cmd.Parameters.AddWithValue("@Sexo", persona.Sexo);
                cmd.Parameters.AddWithValue("@FechaNacimiento", DateTime.Parse(persona.FechaNacimiento));
                cmd.Parameters.AddWithValue("@EstatusActualId", persona.EstatusActualId);
                cmd.Parameters.AddWithValue("@Ingresos", persona.Ingresos);
                cmd.Parameters.AddWithValue("@LocalidadId", persona.LocalidadId);
                cmd.Parameters.AddWithValue("@Sublocalidad", persona.SublocalidadId);
                cmd.Parameters.AddWithValue("@Comentarios", persona.Comentarios);
                cmd.Parameters.AddWithValue("@Foto", persona.Foto);

                using (SqlDataAdapter SqlData = new SqlDataAdapter(cmd))
                {
                    SqlData.Fill(datosBD);

                    if (datosBD.Rows.Count == 1)
                    {
                        exito = int.Parse(datosBD.Rows[0]["Exitoso"].ToString());
                    }
                }
            }

            return exito;
        }
    }
}
