﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/// <summary>
/// Contiene todos los objetos de tranferencia de datos utilizados en la capa de presentación y negocios.
/// </summary>
namespace Evaluacion.DTOs.UI
{
    /// <summary>
    /// Catalogos de opciones para mostrar al usuario
    /// </summary>
    public class Catalogos
    {
        public List<ItemCatalogo> Localidad { get; set; }
        public List<ItemCatalogo> Sublocalidad { get; set; }

    }

    /// <summary>
    /// Items de un catalogo simple, contiene un identificador y una descripción
    /// </summary>
    public class ItemCatalogo
    {
        public string id { get; set; }
        public string descripcion { get; set; }
    }
}
