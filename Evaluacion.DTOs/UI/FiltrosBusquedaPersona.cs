﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Evaluacion.DTOs.UI
{
    /// <summary>
    /// Contiene todos los filtros que se pueden aplicar a la busqueda de personas
    /// </summary>
    public class FiltrosBusquedaPersona
    {
        public string Nombre { get; set; }
        public string FechaInicio { get; set; }
        public string FechaFin { get; set; }
        public char Sexo { get; set; }
        public char Localidad { get; set; }
    }
}
