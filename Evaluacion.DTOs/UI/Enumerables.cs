﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Evaluacion.DTOs.UI
{
    public enum Estatus{
        Estudiando = 1,
        Trabajando = 2,
        Ambos = 3
    }

    public enum Localidades
    {
        Local = 'L',
        Extranjero = 'E'
    }
}
