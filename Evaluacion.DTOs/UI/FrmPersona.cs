﻿using System;
using System.ComponentModel.DataAnnotations;
namespace Evaluacion.DTOs.UI
{
    public class FrmPersona
    {
        public int IdPersona { get; set; }

        public string Nombres { get; set; }
        public string Apellidos { get; set; }
        public char Sexo { get; set; }
        public string FechaNacimiento { get; set; }
        public bool EstatusEstudia { get; set; }
        public bool EstatusTrabaja { get; set; }
        public decimal Ingresos { get; set; }
        public char Localidad { get; set; }
        public string Sublocalidad { get; set; }
        public string Comentarios { get; set; }
        public string Foto { get; set; }
    }
}
