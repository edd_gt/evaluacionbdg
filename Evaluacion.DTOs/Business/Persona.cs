﻿using System;

/// <summary>
/// Contiene los objetos de tranferencia de datos para la capa de negocio y datos.
/// </summary>
namespace Evaluacion.DTOs.Business
{
    /// <summary>
    /// Representa la tabla persona
    /// </summary>
    public class Persona
    {
        public int IdPersona { get; set; }
        public string Nombres { get; set; }
        public string Apellidos { get; set; }
        public char Sexo { get; set; }
        public string FechaNacimiento { get; set; }
        public short EstatusActualId { get; set; }
        public decimal Ingresos { get; set; }
        public char LocalidadId { get; set; }
        public string SublocalidadId { get; set; }
        public string Comentarios { get; set; }
        public string Foto { get; set; }
    }
}
