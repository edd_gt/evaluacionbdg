﻿CREATE PROCEDURE [dbo].[RegistrarPersona]
	@Nombres varchar(50),
	@Apellidos varchar(50),
	@Sexo char(1),
	@FechaNacimiento datetime,
	@EstatusActualId tinyint,
	@Ingresos decimal(6,2),
	@LocalidadId char(1),
	@Sublocalidad char(2),
	@Comentarios varchar(500),
	@Foto varchar(MAX)
AS
	BEGIN TRY
		INSERT INTO Persona (Nombres
			, Apellidos
			, Sexo
			, FechaNacimiento
			, EstatusActualId
			, Ingresos
			, LocalidadId
			, SublocalidadId
			, Comentarios
			, Foto)
			VALUES(
				@Nombres
				, @Apellidos
				, @Sexo
				, @FechaNacimiento
				, @EstatusActualId
				, @Ingresos
				, @LocalidadId
				, @Sublocalidad
				, @Comentarios
				, @Foto)

		SELECT 1 AS Exitoso

	END TRY
	BEGIN CATCH
		
		DECLARE @IdError int = 0
		INSERT INTO DBO.BitacoraErroresBD
		VALUES
		(SUSER_SNAME(),
		ERROR_NUMBER(),
		ERROR_STATE(),
		ERROR_SEVERITY(),
		ERROR_LINE(),
		ERROR_PROCEDURE(),
		ERROR_MESSAGE(),
		GETDATE());

		SELECT  @IdError = SCOPE_IDENTITY()
		
		DECLARE @Message varchar(MAX) = 'Ocurrió un error en Base de datos: IdError: ' + CONVERT(varchar, @IdError),
        @Severity int = ERROR_SEVERITY(),
        @State smallint = ERROR_STATE()

		RAISERROR (@Message, @Severity, @State)

	END CATCH
