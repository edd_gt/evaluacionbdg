﻿CREATE PROCEDURE [dbo].[ConsultarPersona]
	@FiltroId int = NULL,
	@FiltroNombre varchar(100) = '%',
	@FiltroFechaInicio datetime = '1900-01-01 00:00:00.000',
	@FiltroFechaFin datetime = NULL,
	@FiltroSexto char(1) = '%',
	@FiltroLocalidad char(1) = '%'
AS
	BEGIN TRY

		IF(@FiltroId IS NOT NULL)
		BEGIN
			SELECT * 
			FROM Persona
			WHERE IdPersona = @FiltroId
		END
		ELSE
		BEGIN
			IF (@FiltroFechaFin = NULL)
			BEGIN
				SELECT @FiltroFechaFin = GETDATE()
			END

			SELECT *
			FROM Persona
			WHERE concat(Nombres, ' ' , Apellidos) like '%' + @FiltroNombre + '%' 
				and (FechaNacimiento between @FiltroFechaInicio and @FiltroFechaFin)
				and Sexo like @FiltroSexto
				and LocalidadId like @FiltroLocalidad 
		END

	END TRY
	BEGIN CATCH
		
		DECLARE @IdError int = 0
		INSERT INTO DBO.BitacoraErroresBD
		VALUES
		(SUSER_SNAME(),
		ERROR_NUMBER(),
		ERROR_STATE(),
		ERROR_SEVERITY(),
		ERROR_LINE(),
		ERROR_PROCEDURE(),
		ERROR_MESSAGE(),
		GETDATE());

		SELECT  @IdError = SCOPE_IDENTITY()
		
		DECLARE @Message varchar(MAX) = 'Ocurrió un error en Base de datos: IdError: ' + CONVERT(varchar, @IdError),
        @Severity int = ERROR_SEVERITY(),
        @State smallint = ERROR_STATE()

		RAISERROR (@Message, @Severity, @State)

	END CATCH
