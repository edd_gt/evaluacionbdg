﻿CREATE PROCEDURE [dbo].[RegistrarErrorApp]
	-- Add the parameters for the stored procedure here
	@Objeto varchar(500),
	@Metodo varchar(500),
	@Mensaje varchar(1000),
	@StackTrace varchar(MAX)
AS
BEGIN

	BEGIN TRY
		INSERT INTO dbo.BitacoraErroresApp (Objeto, Metodo, Mensaje, StackTrace, FechaRegistro)
			VALUES(@Objeto, @Metodo, @Mensaje, @StackTrace, GETDATE())

		SELECT 1 AS CODE

	END TRY
	BEGIN CATCH
		ROLLBACK TRAN;
		
		DECLARE @IdError int = 0
		INSERT INTO DBO.BitacoraErroresBD
		VALUES
		(SUSER_SNAME(),
		ERROR_NUMBER(),
		ERROR_STATE(),
		ERROR_SEVERITY(),
		ERROR_LINE(),
		ERROR_PROCEDURE(),
		ERROR_MESSAGE(),
		GETDATE());

		SELECT  @IdError = SCOPE_IDENTITY()
		
		DECLARE @Message varchar(MAX) = 'Ocurrió un error en Base de datos: IdError: ' + CONVERT(varchar, @IdError),
        @Severity int = ERROR_SEVERITY(),
        @State smallint = ERROR_STATE()

		RAISERROR (@Message, @Severity, @State)
	END CATCH
END
GO

