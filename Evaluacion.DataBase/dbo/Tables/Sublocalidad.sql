﻿CREATE TABLE [dbo].[Sublocalidad]
(
	[IdSublocalidad] CHAR(2) NOT NULL PRIMARY KEY, 
    [Descripcion] VARCHAR(30) NOT NULL
)
