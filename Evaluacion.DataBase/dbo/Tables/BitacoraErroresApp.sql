﻿CREATE TABLE [dbo].[BitacoraErroresApp](
	[IdError] [int] IDENTITY(1,1) NOT NULL,
	[Objeto] [varchar](500) NULL,
	[Metodo] [varchar](500) NULL,
	[Mensaje] [varchar](1000) NULL,
	[StackTrace] [varchar](max) NULL,
	[FechaRegistro] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[IdError] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

