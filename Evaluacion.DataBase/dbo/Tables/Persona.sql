﻿CREATE TABLE [dbo].[Persona]
(
	[IdPersona] INT NOT NULL IDENTITY(1,1), 
    [Nombres] VARCHAR(100) NOT NULL, 
    [Apellidos] VARCHAR(100) NOT NULL, 
    [Sexo] CHAR NOT NULL, 
    [FechaNacimiento] DATETIME NOT NULL, 
    [EstatusActualId] TINYINT NOT NULL, 
    [Ingresos] DECIMAL(6, 2) NULL, 
    [LocalidadId] CHAR NOT NULL, 
    [SublocalidadId] CHAR(2) NULL, 
    [Comentarios] VARCHAR(500) NULL, 
    [Foto] VARCHAR(MAX) NOT NULL, 
    CONSTRAINT [PK_Persona] PRIMARY KEY (IdPersona), 
    CONSTRAINT [FK_Persona_Estatus ] FOREIGN KEY (EstatusActualId) REFERENCES [Estatus](IdEstatus), 
    CONSTRAINT [FK_Persona_LocalidadId] FOREIGN KEY (LocalidadId) REFERENCES [Localidad]([IdLocalidad]), 
    CONSTRAINT [FK_Persona_Sublocalidad] FOREIGN KEY ([SublocalidadId]) REFERENCES [Sublocalidad]([IdSublocalidad])
)
