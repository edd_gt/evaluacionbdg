﻿CREATE TABLE [dbo].[Localidad]
(
	[IdLocalidad] CHAR NOT NULL PRIMARY KEY, 
    [Descripcion] VARCHAR(30) NOT NULL
)
