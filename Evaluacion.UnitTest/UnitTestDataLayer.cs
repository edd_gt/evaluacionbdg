﻿using System;
using System.Data;
using System.Xml;
using Evaluacion.DataLayer;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Evaluacion.UnitTest
{
    [TestClass]
    public class UnitTestDataLayer
    {
        string conn = "Server=tcp:webdesamimercado.database.windows.net,1433;Initial Catalog=PruebaBdg;Persist Security Info=False;User ID=admindbdesa;Password=9y1u6Ikpo3Ym;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;";
        [TestMethod]
        public void ConsultarCatalogos()
        {
            IDataAccess data = new DataAccess();

            DataSet datos = data.ConsultarCatalogos();

        }

        [TestMethod]
        public void PruebaConfiguraciones()
        {
            string configuracion = string.Empty;
            string nombreConfiguracion = "CadenaConexionBD";

            string _pathConfiguraciones = AppDomain.CurrentDomain.BaseDirectory;
            _pathConfiguraciones += @"\configuraciones.xml";
            XmlDocument xml = new XmlDocument();

            xml.Load(_pathConfiguraciones);

            configuracion = xml.SelectSingleNode("configuraciones/" + nombreConfiguracion).InnerText;
        }
    }
}
