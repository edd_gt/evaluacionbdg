﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Evaluacion.BusinessLayer;
using Evaluacion.DTOs.Business;
using Evaluacion.DTOs.UI;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Evaluacion.UnitTest
{
    [TestClass]
    public class UnitTestDataBusiness
    {
        string conn = "Server=tcp:webdesamimercado.database.windows.net,1433;Initial Catalog=PruebaBdg;Persist Security Info=False;User ID=admindbdesa;Password=9y1u6Ikpo3Ym;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;";

        [TestMethod]
        public void ConsultaCatalogosTest()
        {
            Usuario usuario = new Usuario();

            Catalogos catalogos = usuario.ConsultarCatalogos();
        }

        [TestMethod]
        public void RegistrarUsuarioTest()
        {
            FrmPersona frmPersona = new FrmPersona
            {
                Nombres = "Astrid",
                Apellidos = "Fernandez Cardona",
                FechaNacimiento = "25/09/2020",
                Sexo = 'F',
                Ingresos = 4000.00M,
                Localidad = 'E',
                Sublocalidad = "EE",
                Comentarios = "Excelente persona",
                Foto = "../Content/Fotos/foto-solicitud.jpg",
                EstatusTrabaja = true,
                EstatusEstudia = true,
            };
            Usuario usuario = new Usuario();

            bool respuesta = usuario.RegistrarPersona(frmPersona);
        }

        [TestMethod]
        public void ConsultarPersonaTest()
        {
            Usuario usuario = new Usuario();

            IEnumerable<Persona> enumerable = usuario.ConsultarPersona(new FiltrosBusquedaPersona());
            _ = enumerable.ToList();
        }
    }
}
